---
title: "Hola mundo"
date: 2019-06-11
tags: ["lectura","ocio"]
thumbnail: "img/hello-world.png"
---
  
Éste es un artículo de ejemplo.  
Puedes añadir más editando tu repo. Lo puedes hacer desde [gitea](https://git.sindominio.net) (interfaz web) o si lo prefieres hacer en local puedes trabajar con git en tu terminal.
<!--more-->
Aquí tienes algunas ayudas de cómo trabajar con hugo y tu web estática:

* [Webs estáticas con git](https://sindominio.net/trastienda/manuales/web_git/)
* [Markdown](https://sindominio.net/trastienda/manuales/markdown_intro/)
* [Cómo conectar a mi home (ssh, sftp)](https://sindominio.net/trastienda/manuales/conectar_home/)  

El tema hugo que utilizas al desplegar la web desde lowry es [mainroad](https://github.com/Vimux/Mainroad/search?q=image&type=Issues). Consulta su página para ver cómo cambiar los parámetros de configuración en el fichero *config.toml* de tu repo.
Dos cosas importantes en este tema: 

 * El artículo en cada carpeta tiene el nombre *index.md*, no *_index.md*.
 * Las imágenes de los artículos van dentro de la carpeta de los mismos pero las miniaturas (thumbnail) que defines en cada artículo (imagen destacada) las tienes que incluir en la carpeta */static/* del repositorio. En los manuales señalados tienes más información de cómo cambiar de tema.

![Bienvenido a bordo](welcome-aboard-sign.jpg)
 
